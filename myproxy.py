#!/usr/bin/python
# -*- coding: UTF-8 -*-
import requests,random
from bs4 import BeautifulSoup
def proxy_get():
	ip_list = {}
	headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "zh-CN,zh;q=0.9"
    }
	url = "http://www.xicidaili.com/wn"
	r = requests.get(url,headers=headers)
	soup = BeautifulSoup(r.text,'html.parser')    #解析器：html.parser
	for i in soup.select('#ip_list')[0].select('tr')[1:]:
		ip = i.select('td')[1].string
		port = i.select('td')[2].string
		ip_list[ip] = port
	result = "http://%s:%s" %random.choice(list(ip_list.items()))
	while(proxy_check(result) == False):
		result = "http://%s:%s" %random.choice(list(ip_list.items()))
	return result

def proxy_check(ip_port):
	proxies = {
	  "https": ip_port,
	}
	url = 'https://www.bilibili.com'
	try:
		if requests.get(url,proxies = proxies).status_code == 200:
			print u"%s 可用" %ip_port
			return True
		else:
			print u"%s 不可用" %ip_port
			return False
	except:
		pass

